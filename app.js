const express = require('express');
const path = require('path');
const fs = require('fs');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// Renderización

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.get('/', (req, res) => {
	res.redirect('/inicio');
})

app.get('/info/alumno/:codigo', (req, res) => {
	let codigo = req.params.codigo;
	let data = fs.readFileSync('alumnos.json', 'utf8');
	let alumnoEncontrado = JSON.parse(data)[codigo];
	res.render('info', {
		info: alumnoEncontrado,
		randomValue: Math.floor(Math.random()*10)+1
	});
})

app.get('/inicio', (req, res) => {
	res.render('index');
})

app.listen(process.env.PORT || 3000, () => {
	console.log('Server running on port 3000');
})